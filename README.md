# Insecurity Ubuntu docker base image #
Autobuilded `ubuntu:xenial` up-to-date image

### About ###

Dockerfile based on official `ubuntu` repository to build an up-to-date `ubuntu:xenial` baseimage (apt-get dist-upgrade) with a couple of extra packages. 

The image is built on top of the most recently tagged `ubuntu:xenial` image and installs the following extra packages:

 - ca-certificate
 - unzip
 - wget
 - vim-tiny

When the official `ubuntu` repository is updated, this image is automatically rebuilt. Moreover, a crontab job on my server checks 3x/day and rebuilds this image if new updates are available.
 
Additionally apt is configured to NOT install `recommended` or `suggested` packages and with `Acquire::Retries` to 5 if a package download fails.
